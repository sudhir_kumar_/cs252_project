# README #

MY NOTE
CS252 PROJECT BY GROUP 49
Members:
AKSHAT ANAND (14055)
POGIRI KALA SAGAR (14466)
SUDHIR KUMAR (14716)

### FEATURES ###

* Avoid making your walls a paper portrait by our note feature.
* Keep track of events by scheduling it on our scheduler.
* Upload your files without worrying about file types.
* Login logout feature to help you facilitate between several accounts and maintain security.
* Quick Access to various features with one click navigation bar.

### TECHNOLOGY USED ###

* The website is based on latest Django framework based on Python.
* Data is being managed by Sqlite database as default database of Django.
* The backend functionalities are defined in Python language.
* The front end development uses HTML, CSS and bootstrap.

### LOGIN LOGOUT ###

* This app provides the invisible barrier between various users.
* It has model for UserInfo which stores information about users and maintain their privacy.
* The UserInfo model has fields for  Actual Name, Username, Gender, Profile Pic,  password and defined in models.py file.
* For maintaining the continuity, it uses HTML pages for Register, Login.
* It maintains the database and user access through definitions like Register_NewUser, LoginUser, LogoutUser, etc.
* These are defined in views.py file.

### NOTES ###
* This app takes care of cloud space and upload of files, managing notes, scheduling events and searching the user space.
* It has various models named Notes, Event, Document and WorkOut Calendar and these are defined in model.py.
* These models have field to maintain note tags, notes, event tags , descriptions, the dates concerning these notes and events and finally field for storing document of various types.
* For continuity,  it has various HTML templates assisted by CSS and BootStrap for adding notes, events and documents and to view the already added notes, events and documents.
* It maintains the database and facilitates space through definitions like CreateNewNote, CreateNewEvent, etc which are defined in views.py file
* Finally, it has search property which enables you to search notes, events and documents through their tags, names or concerning dates.